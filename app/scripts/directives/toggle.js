'use strict';

/* Converted to directive from http://codepen.io/mikebarwick/pen/kCtgj 
*
*/

angular.module('directive.toggle',[]).directive('toggle', ['$window', function($window) {

	return {
		restrict: 'A',
		link: function (scope, element, attrs, ctrl, linker){
		
			if (attrs.toggle == 'tooltip') {
				$(element).tooltip();
			}			

			if (attrs.toggle == 'popover') {
				$(element).popover({
					html:true,
					content: function(){
						return $(this).prev().html();
					}
				});
			}			

		}//link - function
	};//return
}]);//directive
