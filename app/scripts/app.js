'use strict';

/**
 * @ngdoc overview
 * @name categorizerFrontendApp
 * @description
 * # categorizerFrontendApp
 *
 * Main module of the application.
 */
angular
  .module('categorizerFrontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
		'firebase',
		'ui.tree',
		'directive.toggle'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html'
        //templateUrl: 'views/main.html',
        //controller: 'MainCtrl',
        //controllerAs: 'main'
      })
			.when('/admin', {
				templateUrl: 'views/admin.html'	
			})
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
