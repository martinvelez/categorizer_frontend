'use strict';

/**
 * @ngdoc function
 * @name categorizerFrontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the categorizerFrontendApp
 */
angular.module('categorizerFrontendApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
