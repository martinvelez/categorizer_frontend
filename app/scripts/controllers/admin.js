'use strict';


/**
 * @ngdoc function
 * @name categorizerFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the categorizerFrontendApp
 */
angular.module('categorizerFrontendApp')
.controller('AdminCtrl', ['$scope', '$firebaseAuth', '$firebaseObject', function
($scope, $firebaseAuth, $firebaseObject) {
	console.log('AdminCtrl');
	var usersRef = new Firebase("https://brilliant-fire-5347.firebaseio.com/users");
	//var auth = $firebaseAuth(usersRef);
	$scope.users = {};
	$scope.user_selected_uid = 'None';
	$scope.userCount = 0;
	$scope.usersLoaded = false;

	usersRef.orderByChild("first_name").on("child_added", function(snapshot, prevChildKey){
		console.log("child_added");
		$scope.userCount++;
		var newUser = snapshot.val();
		$scope.users[newUser.uid] = newUser;
	});

	usersRef.on("value", function(snapshot){
		$scope.usersLoaded = true;
		$scope.$apply();
	}, function (errorObject) {
		console.log("The read failed: " + errorObject.code);
	});	
	
	$scope.change = function(){
		console.log('change()');
		$scope.userSelected = $scope.users[$scope.user_selected_uid];
		$scope.userSelected.date = new Date($scope.userSelected.date);
	};


}]);
